package tk.piotrek54pl.mcnext.updater;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.text.StrSubstitutor;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import tk.piotrek54pl.mcnext.LauncherConstants;
import tk.piotrek54pl.mcnext.Logger;
import tk.piotrek54pl.mcnext.MCNext;
import tk.piotrek54pl.mcnext.OS;
import tk.piotrek54pl.mcnext.Utils;

public class MinecraftDownload
{
	List<Version> versions = new ArrayList<Version>();
	
	public List<Version> getVersions()
	{
		return versions;
	}
	
	public Version downloadVersion(String id)
	{
		try {
			Version v = getVersion(id);
			if(!v.isDownloaded()) {
				if(v.getType().equals("forge")) {
					ForgeInstaller.install(v);
				} else {
					URL url = new URL("https://s3.amazonaws.com/Minecraft.Download/versions/" + id + "/" + id + ".json");
				    File json = new File(Utils.getWorkingDirectory(), "versions" + File.separator + id + File.separator + id + ".json");
				    Utils.downloadFile(json, url);
				}
			    
			    update();
			    checkMissingFiles();
			    v = getVersion(id);
			} else {
				checkMissingFiles();
			}
			
			return v;
		} catch(IOException | InterruptedException e) {
			if(MCNext.debugMode) e.printStackTrace();
			Logger.popupError("Krytyczny b��d!", 
	    			"Z powodu nieoczekiwanego b��du nie uda�o si� pobra� informacji na temat wersji. Powiadom nas o tym!", 
	    			e.getMessage());
		}
		return null;
	}
	
	private Version getVersion(String id) {
		Iterator<Version> i = versions.iterator();
		while(i.hasNext()) {
			Version v = i.next();
			if(v.getId().equals(id)) {
				return v;
			}
		}
		return null;
	}
	
	
	public void update()
	{
		try {
			versions.clear();
			List<Version> local = checkLocalVersions();
			
			if(!MCNext.offlineMode) {
				MCNext.frame.setMessage("Synchronizacja wersji minecraft");
				Iterator<Version> remoteMCi = checkRemoteVersions().iterator();
				while(remoteMCi.hasNext()) {
					Version newVersion = remoteMCi.next();
					
					Iterator<Version> li = local.iterator();
					while(li.hasNext()) {
						Version localVersion = li.next();
						if(newVersion.getId().equals(localVersion.getId())) {
							newVersion.setDownloaded(true);
							localVersion.setDownloaded(true);
							newVersion.setMinecraftArguments(localVersion.getMinecraftArguments());
							newVersion.setLibraries(localVersion.getLibraries());
							newVersion.setMainClass(localVersion.getMainClass());
						}
					}
					versions.add(newVersion);
				}
				
				MCNext.frame.setMessage("Synchronizacja wersji forge");
				Iterator<Version> remoteMCForgei = checkRemoteForgeVersions().iterator();
				while(remoteMCForgei.hasNext()) {
					Version newVersion = remoteMCForgei.next();
					
					Iterator<Version> li = local.iterator();
					while(li.hasNext()) {
						Version localVersion = li.next();
						if(newVersion.getId().equals(localVersion.getId())) {
							newVersion.setDownloaded(true);
							localVersion.setDownloaded(true);
							newVersion.setMinecraftArguments(localVersion.getMinecraftArguments());
							newVersion.setLibraries(localVersion.getLibraries());
							newVersion.setMainClass(localVersion.getMainClass());
						}
					}
					
					versions.add(newVersion);
				}
				

			}
			
			Iterator<Version> li = local.iterator();
			while(li.hasNext()) {
				Version v = li.next();
				
				if(!v.isDownloaded()) {
					v.setDownloaded(true);
					versions.add(v);
				}
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		
	}
	

	@SuppressWarnings("unchecked")
  	private List<Version> checkRemoteVersions() throws IOException, ParseException {
	    URL remoteVersionsURL = new URL(LauncherConstants.DOWNLOAD_URL+"versions/versions.json");
	    JSONObject remoteVersionsJSON = (JSONObject)new JSONParser().parse(new InputStreamReader(remoteVersionsURL.openStream()));
	    Iterator<JSONObject> remoteVersionsIterator = ((JSONArray)remoteVersionsJSON .get("versions")).iterator();
	    
	    List<Version> remoteVersions = new ArrayList<Version>();
	    
	    while(remoteVersionsIterator.hasNext())
	    {
	    	JSONObject versionJSON = remoteVersionsIterator.next();
	    	Version remoteVersion = new Version();
	    	
	    	remoteVersion.setId((String)versionJSON.get("id"));
	    	remoteVersion.setTime((String)versionJSON.get("time"));
	    	remoteVersion.setReleaseTime((String)versionJSON.get("releaseTime"));
	    	remoteVersion.setType((String)versionJSON.get("type"));
		
		    remoteVersions.add(remoteVersion);
	    }
	    
	    Logger.info("Zsynchronizowano zdaln� list� wersji. ("+remoteVersions.size()+")");
	    return remoteVersions;
	}
	
	private List<Version> checkRemoteForgeVersions() throws IOException {
		Document doc = Jsoup.connect("http://files.minecraftforge.net/minecraftforge/index_legacy.html").get();
		Elements list = doc.select(".promotions table tr");
		List<Version> remoteVersions = new ArrayList<Version>();
		
		for(int i=0;i < list.size();i++) {
			Elements elements = list.get(i).getElementsByTag("td");
			if(elements.size() > 0 && elements.get(0).text().startsWith("Recommended-") && !elements.get(0).text().contains("1.5.2")) {
				Version v = new Version();
				v.setId(elements.get(2).text()+"-Forge"+elements.get(1).text());
				v.setTime(elements.get(3).text());
				v.setForgeUniversalUrl(elements.get(4).select("a").get(8).attr("abs:href"));
				v.setType("forge");
				
				remoteVersions.add(v);
			}
		}
		
		return remoteVersions;
	}

	@SuppressWarnings("unchecked")
	private List<Version> checkLocalVersions() throws FileNotFoundException, IOException, ParseException {
		File versionsFolder = new File(Utils.getWorkingDirectory(), "versions");
		versionsFolder.getParentFile().mkdirs();
		File[] versionFolders = versionsFolder.listFiles();

		List<Version> localVersions = new ArrayList<Version>();
		if (versionFolders == null) return localVersions;
		
		for (File folder : versionFolders) { if (folder.isDirectory()) {
			File jsonfile = new File(folder.getAbsolutePath() + File.separator + folder.getName() + ".json");
			if(jsonfile.exists()) {
				JSONObject key = (JSONObject)new JSONParser().parse(new FileReader(jsonfile));
				Version v = new Version();
			
				v.setId((String)key.get("id"));
				v.setTime((String)key.get("time"));
				v.setReleaseTime((String)key.get("releaseTime"));
				v.setType((String)key.get("type"));
				v.setMinecraftArguments((String)key.get("minecraftArguments"));
				v.setMainClass((String)key.get("mainClass"));
				
				if(((String)key.get("id")).contains("Forge")) v.setType("forge");
            
				Iterator<JSONObject> libs = ((JSONArray)key.get("libraries")).iterator();
	              while (libs.hasNext()) {
	                JSONObject jlib = libs.next();
	                Library lib = new Library();
	                lib.setName((String)jlib.get("name"));

	                Set<OS> allowed = new HashSet<OS>();
	                if (jlib.containsKey("rules")) {
	                  Iterator<JSONObject> rules = ((JSONArray)jlib.get("rules")).iterator();
	                  while (rules.hasNext()) {
	                    JSONObject rule = (JSONObject)rules.next();
	                    if ((rule.get("action").equals("allow")) && (rule.containsKey("os"))) {
	                      if (((JSONObject)rule.get("os")).get("name").equals("windows")) allowed.add(OS.WINDOWS);
	                      if (((JSONObject)rule.get("os")).get("name").equals("linux")) allowed.add(OS.LINUX);
	                      if (((JSONObject)rule.get("os")).get("name").equals("osx")) allowed.add(OS.MACOS);
	                    }
	                    if ((rule.get("action").equals("allow")) && (!rule.containsKey("os"))) {
	                      allowed.add(OS.WINDOWS);
	                      allowed.add(OS.LINUX);
	                      allowed.add(OS.MACOS);
	                    }
	                    if ((rule.get("action").equals("disallow")) && (rule.containsKey("os"))) {
	                      if (((JSONObject)rule.get("os")).get("name").equals("windows")) allowed.remove(OS.WINDOWS);
	                      if (((JSONObject)rule.get("os")).get("name").equals("linux")) allowed.remove(OS.LINUX);
	                      if (!((JSONObject)rule.get("os")).get("name").equals("osx")) continue; allowed.remove(OS.MACOS);
	                    }
	                  }
	                } else {
	                  allowed.add(OS.WINDOWS);
	                  allowed.add(OS.LINUX);
	                  allowed.add(OS.MACOS);
	                }

	                if (jlib.containsKey("natives")) {
	                	JSONObject n = (JSONObject) jlib.get("natives");
	                	if(n.containsKey(Utils.getOperatingSystem().getName())) {
	                		lib.setContainsNatives(true);
	                		String nativesString = (String) n.get(Utils.getOperatingSystem().getName());
	                		Map<String,String> map = new HashMap<String,String>();
	                		map.put("arch", System.getProperty("sun.arch.data.model"));
	                		
	                		StrSubstitutor sub = new StrSubstitutor(map);
	                		lib.setNativesString(sub.replace(nativesString));
	                	}
	                }
	                
	                if(jlib.containsKey("url")) {
	                	lib.setUrl((String)jlib.get("url"));
	                }

	                lib.setTargetOS(allowed);
	                
	                v.addLibrary(lib);
	              }
	              
	           localVersions.add(v);
			}
		}}
		
		Logger.info("Zsynchronizowano lokaln� list� wersji. ("+localVersions.size()+")");
		return localVersions;
	}

  private void checkMissingFiles() throws MalformedURLException, InterruptedException
  {
	  Downloader filesToDownload = new Downloader();
	  Iterator<Version> versionsIterator = versions.iterator();
	    
	  while (versionsIterator.hasNext()) {
		  Version ver = versionsIterator.next();
	      if(ver.isDownloaded()) {
	    	  File versionJar = new File(Utils.getWorkingDirectory(), "versions" + File.separator + ver.getId() + File.separator + ver.getId() + ".jar");
	    	  if (!versionJar.exists() && !ver.getType().equals("forge")) {
	    		  URL url = new URL(LauncherConstants.DOWNLOAD_URL + "versions/" + ver.getId() + "/" + ver.getId() + ".jar");
	    		  filesToDownload.addFile(new Downloadable(url, versionJar));
		      }
	    	  
	    	  Iterator<Library> librariesIterator = ver.getLibraries().iterator();
	          while (librariesIterator.hasNext()) {
	              Library lib = librariesIterator.next();
	              
	              String URL = LauncherConstants.DOWNLOAD_URL+"libraries/";
	              if(lib.getUrl() != null) URL = "http://repo1.maven.org/maven2/";
	              
	              if (lib.getTargetOS().contains(Utils.getOperatingSystem())) {
		                String[] parts = lib.getName().split(":");
		                String dir = parts[0].replace(".", File.separator) + File.separator + parts[1] + File.separator + parts[2];
		                String filename;
		                if (lib.containsNatives())
		                	filename = parts[1] + "-" + parts[2] + "-" +lib.getNativesString()+ ".jar";
		                else {
		                	filename = parts[1] + "-" + parts[2] + ".jar";
		                }
		                if (!new File(Utils.getWorkingDirectory(), "libraries/" + dir + File.separator + filename).exists()) {
			                URL url = new URL(URL + dir.replace(File.separator, "/") + "/" + filename);
			                File file = new File(Utils.getWorkingDirectory(), "libraries" + File.separator + dir + File.separator + filename);
			                
			                Logger.debug("DL: "+ URL + dir.replace(File.separator, "/") + "/" + filename);
			                
			                System.out.println(url.getHost()+url.getPath());
			                filesToDownload.addFile(new Downloadable(url, file));
		                }
	                }
	          }
	      }
	  }
	  
	  filesToDownload.start();
      MCNext.frame.setMessage("Aktualizacja bibliotek");
      while (!filesToDownload.finished) Thread.sleep(10L); 
  }
}