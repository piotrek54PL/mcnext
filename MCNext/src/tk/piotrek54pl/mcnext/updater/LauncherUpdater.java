package tk.piotrek54pl.mcnext.updater;

import com.alee.extended.label.WebLinkLabel;
import com.alee.extended.panel.CenterPanel;
import com.alee.extended.panel.GroupPanel;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.managers.notification.NotificationIcon;
import com.alee.managers.notification.NotificationManager;
import com.alee.managers.notification.WebNotificationPopup;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import tk.piotrek54pl.mcnext.LauncherConstants;
import tk.piotrek54pl.mcnext.Logger;
import tk.piotrek54pl.mcnext.MCNext;
import tk.piotrek54pl.mcnext.Utils;

public class LauncherUpdater
{
  public void updateLauncher()
  {
    try
    {
      String content = Utils.getURLContent("http://mc-next.tk/download.php?action=version");

      String[] parts = content.split("<#>");
      int latestVersion = Integer.parseInt(parts[0].replace(".", ""));
      int runningVersion = LauncherConstants.getNumericVersion();
      if (latestVersion > runningVersion) {
    	  if(latestVersion - runningVersion <= 3) {
    		  	MCNext.frame.setTip("Aktualizacja launchera!");
    		  	Logger.info("Dost�pna jest nowa wersja launchera!");
    	        WebNotificationPopup notificationPopup = new WebNotificationPopup();
    	        notificationPopup.setIcon(NotificationIcon.question);
    	        JLabel label = new JLabel("Dost�pna jest nowa aktualizacja launchera!");
    	        WebLinkLabel link = new WebLinkLabel();
    	        link.setLink(parts[1]);
    	        link.setIcon(null);
    	        link.setText("Pobierz");

    	        CenterPanel centerPanel = new CenterPanel(link, false, true);
    	        notificationPopup.setContent(new GroupPanel(2, new Component[] { label, centerPanel }));
    	        NotificationManager.showNotification(notificationPopup);
    	  } else {
    		  String[] options = {"Pobierz","Wyjd�"};
    		  int code = WebOptionPane.showOptionDialog(null, 
    			         "Tw�j launcher jest przestarza�y! Pobierz now� wersj�.", 
    			         "Przestarza�y launcher", 0, JOptionPane.INFORMATION_MESSAGE, 
    			         null, options, "Pobierz");
    		  if(code == 0) { Utils.openURI(parts[1]); }
    		  MCNext.exit();
    	  }
      }
      else {
        Logger.info("Brak nowej wersji launchera. (" + (((Boolean)MCNext.prefs.get("downloadBetaLauncher")).booleanValue() ? "Beta" : "Stable") + ")");
      }
    } catch (Exception e) {
    	Logger.error("Nie uda�o si� sprawdzi� aktualizacji launchera! (" + e.getMessage() + ")");
    }
  }
}