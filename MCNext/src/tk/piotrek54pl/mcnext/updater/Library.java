package tk.piotrek54pl.mcnext.updater;

import java.io.File;
import java.util.HashSet;
import java.util.Set;
import tk.piotrek54pl.mcnext.OS;
import tk.piotrek54pl.mcnext.Utils;

public class Library
{
  private String name;
  private boolean containsNatives;
  private String nativesString;
  private Set<OS> targetOS = new HashSet<OS>();
  private File file;
  private String url;

  public String getName()
  {
    return this.name;
  }
  public void setName(String name) {
    this.name = name;
  }
  
  public String getNativesString() {
	return nativesString;
}
public void setNativesString(String nativesString) {
	this.nativesString = nativesString;
}
public boolean containsNatives() {
    return this.containsNatives;
  }
  public void setContainsNatives(boolean containsNatives) {
    this.containsNatives = containsNatives;
  }
  public Set<OS> getTargetOS() {
    return this.targetOS;
  }
  public void setTargetOS(Set<OS> targetOS) {
    this.targetOS = targetOS;
  }
  public void setFile(File f) {
    this.file = f;
  }
  public File getFile() {
    if (this.file == null) {
      String[] parts = this.name.split(":");
      String dir = parts[0].replace(".", File.separator) + File.separator + parts[1] + File.separator + parts[2];
      String filename;
      if (this.containsNatives)
        filename = parts[1] + "-" + parts[2] + "-" +nativesString+ ".jar";
      else {
        filename = parts[1] + "-" + parts[2] + ".jar";
      }
      this.file = new File(Utils.getWorkingDirectory(), "libraries" + File.separator + dir + File.separator + filename);
    }
    return this.file;
  }
  public void setUrl(String url) {
	  this.url = url;
  }
  public String getUrl() {
	  return url;
  }
}