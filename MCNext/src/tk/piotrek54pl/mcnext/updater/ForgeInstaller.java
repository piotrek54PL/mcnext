package tk.piotrek54pl.mcnext.updater;

import java.io.File;
import java.net.URL;

import tk.piotrek54pl.mcnext.Logger;
import tk.piotrek54pl.mcnext.MCNext;
import tk.piotrek54pl.mcnext.Utils;

public class ForgeInstaller {
	
	public static void install(Version v) {
		try {
			MCNext.frame.setMessage("Pobieranie wersji Forge");
			
			URL url = new URL(v.getForgeUniversalUrl());
			String[] parts = url.getFile().replace(".jar","").split("-");
			String filename = "minecraftforge-"+parts[3]+".jar";
		    File forgeLib = new File(Utils.getWorkingDirectory(), "libraries"+File.separator+"net"+
		    													  File.separator+"minecraftforge"+
		    													  File.separator+"minecraftforge"+
		    													  File.separator+parts[3]+File.separator+
		    													  filename);
		    Utils.downloadFile(forgeLib, url);
		    
		    Utils.unzipFile(forgeLib, "version.json", new File(Utils.getWorkingDirectory(),"versions"+File.separator+
		    													v.getId()+File.separator+v.getId()+".json"));
		    Version baseVersion = MCNext.md.downloadVersion(parts[2]);
		    
		    Utils.copyFile(new File(Utils.getWorkingDirectory(),"versions"+File.separator+
		    					baseVersion.getId()+File.separator+baseVersion.getId()+".jar"),
		    			   new File(Utils.getWorkingDirectory(),"versions"+File.separator+
		    					v.getId()+File.separator+v.getId()+".jar"));
		    
		    Logger.debug("Zainstalowano wersj� forge: "+v.getId());
		} catch (Exception e) {
			if(MCNext.debugMode) e.printStackTrace();
		}
	}
}
