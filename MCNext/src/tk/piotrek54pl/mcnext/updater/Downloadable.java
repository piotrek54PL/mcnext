package tk.piotrek54pl.mcnext.updater;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import tk.piotrek54pl.mcnext.MCNext;

public class Downloadable
{
  private final URL url;
  private final File target;
  public boolean downloaded = false;

  public Downloadable(URL remoteFile, File localFile)
  {
    this.url = remoteFile;
    this.target = localFile;
  }

  public void download()
  {
	  try {
		  URLConnection conn = url.openConnection();
		  conn.addRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
		  conn.connect();
		  InputStream is = conn.getInputStream();

		  if (!this.target.getParentFile().exists()) this.target.getParentFile().mkdirs();
	      if (!this.target.exists()) this.target.createNewFile();
	      FileOutputStream fos = new FileOutputStream(this.target);
	
	      byte[] buffer = new byte[65536];
	      int bytesRead = 0;
	
	      while ((bytesRead = is.read(buffer)) != -1) {
	        fos.write(buffer, 0, bytesRead);
	      }
	
	      if (fos != null) fos.close();
	      if (is != null) is.close();
	  } catch (IOException e) {
		  if(MCNext.debugMode) e.printStackTrace();
		  //Logger.popupError("Krytyczny b��d!", "Nie uda�o si� pobra� pliku z powodu nieoczekiwanego b��du. By� mo�e serwery s� przeci��one.", e.getMessage(), false);
	  }
	  this.downloaded = true;
  }
}