package tk.piotrek54pl.mcnext.updater;

import java.util.HashSet;
import java.util.Set;

public class Version
{
  String id;
  String time;
  String releaseTime;
  String type;
  String minecraftArguments;
  Set<Library> libraries;
  String mainClass;
  boolean downloaded;
  
  String forgeUniversalUrl;

public String getForgeUniversalUrl() {
	return forgeUniversalUrl;
}

public void setForgeUniversalUrl(String forgeUniversalUrl) {
	this.forgeUniversalUrl = forgeUniversalUrl;
}

public Version()
  {
	  this.libraries = new HashSet<Library>();
  }

  public Version(String id, String time, String releaseTime, String type)
  {
    this.id = id;
    this.time = time;
    this.releaseTime = releaseTime;
    this.type = type;

    this.libraries = new HashSet<Library>();
  }

  public String getId() {
    return this.id;
  }
  public void setId(String id) {
    this.id = id;
  }
  public String getTime() {
    return this.time;
  }
  public void setTime(String time) {
    this.time = time;
  }
  public String getReleaseTime() {
    return this.releaseTime;
  }
  public void setReleaseTime(String releaseTime) {
    this.releaseTime = releaseTime;
  }
  public String getType() {
    return this.type;
  }
  public void setType(String type) {
    this.type = type;
  }
  public String getMinecraftArguments() {
    return this.minecraftArguments;
  }
  public void setMinecraftArguments(String minecraftArguments) {
    this.minecraftArguments = minecraftArguments;
  }
  public Set<Library> getLibraries() {
    return this.libraries;
  }
  public void addLibrary(Library lib) {
    this.libraries.add(lib);
  }
  public String getMainClass() {
    return this.mainClass;
  }
  public void setMainClass(String mainClass) {
    this.mainClass = mainClass;
  }
  public void setDownloaded(boolean downloaded){
	  this.downloaded = downloaded;
  }
  public boolean isDownloaded()
  {
	  return downloaded;
  }
  public void setLibraries(Set<Library> libs) {
	  this.libraries = libs;
  }
  
}