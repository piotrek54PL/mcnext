package tk.piotrek54pl.mcnext.updater;

import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import tk.piotrek54pl.mcnext.MCNext;

public class Downloader
{
	Set<Downloadable> files = new HashSet<Downloadable>();
	boolean finished = false;

	public void addFile(Downloadable file)
	{
		this.files.add(file);
	}

	public int size() {
		return files.size();
	}

  public void start() {
	  	try { while (MCNext.frame == null)Thread.sleep(10L); } catch (Exception e) {} 
	    MCNext.frame.switchToWorking();
	    
	    ExecutorService pool = Executors.newFixedThreadPool(24);
	    for (final Downloadable dl : files)
	      pool.submit(new Runnable()
	      {
	        public void run() {
				dl.download();
	        }
	      });
	    pool.shutdown();
	    
	    
	    
	    
	    new Timer().scheduleAtFixedRate(new TimerTask()
	    {
	      public void run() {
	        Iterator<Downloadable> i = Downloader.this.files.iterator();
	        float downloaded = 0.0F;
	        while (i.hasNext()) {
	          downloaded += (((Downloadable)i.next()).downloaded ? 1 : 0);
	        }
	        MCNext.frame.setProgress(Math.round(downloaded / Downloader.this.files.size() * 100.0F));
	        if (Downloader.this.files.size() - downloaded == 0.0F) {
	          cancel();
	          Downloader.this.finished = true;
	        }
	      }
	    }
	    , new Date(), 100L);
	}
}