package tk.piotrek54pl.mcnext.updater;

import java.io.File;
import java.net.URL;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import tk.piotrek54pl.mcnext.LauncherConstants;
import tk.piotrek54pl.mcnext.Logger;
import tk.piotrek54pl.mcnext.MCNext;
import tk.piotrek54pl.mcnext.Utils;

public class MinecraftResources
{
  public void updateResources()
  {
	  MCNext.frame.setMessage("Synchronizacja resources");
	  
    Set<String> resourcesToDownload = new HashSet<String>();
    int totalSize = 0;
    try {
      URL resourceUrl = new URL(LauncherConstants.RESOURCES_URL);
      DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
      Document doc = db.parse(resourceUrl.openStream());
      NodeList nodeLst = doc.getElementsByTagName("Contents");
      
      for (int i = 0; i < nodeLst.getLength(); i++) {
        Node node = nodeLst.item(i);
        if (node.getNodeType() == 1) {
          Element element = (Element)node;
          
          String key = element.getElementsByTagName("Key").item(0).getChildNodes().item(0).getNodeValue();
          long size = Long.parseLong(element.getElementsByTagName("Size").item(0).getChildNodes().item(0).getNodeValue());

          if (size > 0L) {
            File file = new File(Utils.getWorkingDirectory(), "assets" + File.separator + key);
            if ((!file.exists()) || (file.length() != size)) {
              resourcesToDownload.add(key);
              totalSize = (int)(totalSize + size);
            }
          }
        }
      }
    } catch (Exception e) {
    	if(MCNext.debugMode) e.printStackTrace();
    	Logger.popupError("Krytyczny b��d!", 
    			"Wyst�pi� nieoczekiwany b��d, nie uda�o si� pobra� listy resources z serwera, by� mo�e serwery s� przeci��one.", 
    			e.getMessage(), false);
    }

    if (resourcesToDownload.size() == 0) {
      Logger.debug("Brak resources do pobrania, pomijam.");
    } else {
	    Logger.info("Pobieranie " + resourcesToDownload.size() + " plik�w (" + totalSize / 1024 + "KB)");
	    double sizeInMb = Math.round(totalSize / 1048576.0D * 100.0D) / 100.0D;
	    MCNext.frame.setMessage("Pobieranie " + resourcesToDownload.size() + " plik�w (" + sizeInMb + "MB)");
	    try {
	      Iterator<String> i = resourcesToDownload.iterator();
	      Downloader dl = new Downloader();
	      while (i.hasNext())
	      {
	        String key = (String)i.next();
	        URL url = new URL(LauncherConstants.RESOURCES_URL+ key);
	        File file = new File(Utils.getWorkingDirectory(), "assets" + File.separator + key.replace("/", File.separator));
	        dl.addFile(new Downloadable(url, file));
	      }
	      dl.start();
	      while (!dl.finished)
	        Thread.sleep(100L);
	    }
	    catch (Exception e)
	    {
	    	if(MCNext.debugMode) e.printStackTrace();
	    	Logger.popupError("Krytyczny b��d!", 
	    			"Wyst�pi� nieoczekiwany b��d, nie uda�o si� pobra� brakuj�cych resources z serwera. Powiadom nas o tym!", 
	    			e.getMessage());
	    }
    }
    
    Utils.copyFolder(new File(Utils.getWorkingDirectory()+File.separator+"assets"+File.separator+"music"),
    				 new File(Utils.getWorkingDirectory()+File.separator+"assets"+File.separator+"sounds"+File.separator+"music"));
    
    Utils.copyFolder(new File(Utils.getWorkingDirectory()+File.separator+"assets"+File.separator+"records"),
			 new File(Utils.getWorkingDirectory()+File.separator+"assets"+File.separator+"sounds"+File.separator+"records"));
    
    Utils.copyFolder(new File(Utils.getWorkingDirectory()+File.separator+"assets"+File.separator+"sound"+File.separator+"note"),
			 new File(Utils.getWorkingDirectory()+File.separator+"assets"+File.separator+"sounds"+File.separator+"note"));
    
    Utils.copyFolder(new File(Utils.getWorkingDirectory()+File.separator+"assets"+File.separator+"sound"+File.separator+"random"),
			 new File(Utils.getWorkingDirectory()+File.separator+"assets"+File.separator+"sounds"+File.separator+"random"));
    
    Utils.copyFolder(new File(Utils.getWorkingDirectory()+File.separator+"assets"+File.separator+"sound"+File.separator+"step"),
			 new File(Utils.getWorkingDirectory()+File.separator+"assets"+File.separator+"sounds"+File.separator+"step"));
    
    Utils.copyFolder(new File(Utils.getWorkingDirectory()+"assets"+File.separator+"sound"+File.separator+"tile"),
			 new File(Utils.getWorkingDirectory()+File.separator+"assets"+File.separator+"sounds"+File.separator+"tile"));
  }
}