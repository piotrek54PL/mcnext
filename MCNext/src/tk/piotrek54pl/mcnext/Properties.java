package tk.piotrek54pl.mcnext;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Properties
{
  private File f;
  private JSONObject obj;

  public Properties()
  {
	  this.f = new File(Utils.getWorkingDirectory(), "mcnext.json");
	  this.obj = new JSONObject();

	  if (!this.f.exists()) {
	  try {
		  this.f.createNewFile();
	  } catch (IOException e) { 
		  if(MCNext.debugMode)e.printStackTrace();
		  Logger.popupError("Krytyczny b��d!", "Nie uda�o si� utworzy� pliku konfiguracyjnego z powodu nieoczekiwanego b��du. Powiadom nas o tym!", e.getMessage());
	  }
	  }
  }

  public void write()
  {
	  try {
		  FileWriter file = new FileWriter(this.f);
		  file.write(this.obj.toJSONString());
		  file.flush();
		  file.close();
	  } catch (IOException e) {
		  if(MCNext.debugMode)e.printStackTrace(); 
		  Logger.popupError("Krytyczny b��d!", "Nie uda�o si� zapisa� pliku konfiguracyjnego z powodu nieoczekiwanego b��du. Powiadom nas o tym!", e.getMessage());
	  }
  }
  
  @SuppressWarnings("unchecked")
  private void writeDefaults(Object key)
  {
	  if(key instanceof String) { 
		  switch((String)key) {
		  		case "username":
		  			obj.put("username", "Gracz");
		  			break;
		  		case "memory":
		  			obj.put("memory", 1024);
		  			break;
		  		case "selectedVersion":
		  			obj.put("selectedVersion", "");
		  			break;
		  		case "width":
		  			obj.put("width", 854);
		  			break;
		  		case "height":
		  			obj.put("height", 480);
		  			break;
		  		case "checkResources":
		  			obj.put("checkResources", true);
		  			break;
		  		case "checkLauncherUpdate":
		  			obj.put("checkLauncherUpdate", true);
		  			break;
		  		case "forceOfflineMode":
		  			obj.put("forceOfflineMode", false);
		  			break;
		  		case "javaDirectory":
		  			obj.put("javaDirectory", "");
		  			break;
		  		case "minecraftDirectory":
		  			obj.put("minecraftDirectory", "");
		  			break;
		  		case "demo":
		  			obj.put("demo", false);
		  		case "showReleaseVersions":
		  			obj.put("showReleaseVersions", true);
		  		case "showSnapshotVersions":
		  			obj.put("showSnapshotVersions", false);
		  		case "showBetaVersions":
		  			obj.put("showBetaVersions", false);
		  		case "showAlphaVersions":
		  			obj.put("showAlphaVersions", false);
		  		case "showForgeVersions":
		  			obj.put("showForgeVersions", true);
		  		case "debugMode":
		  			obj.put("debugMode", false);
		  			break;
		  }
		  write();
	  }
  }

  public void read() {
    try {
    	BufferedReader br = new BufferedReader(new FileReader(f));     
    	if (br.readLine() != null) {
    	    br.close();
    	    obj = ((JSONObject)new JSONParser().parse(new FileReader(this.f)));
    	}
    } catch (FileNotFoundException e) {
    	if(MCNext.debugMode) e.printStackTrace();
    	Logger.popupError("Krytyczny b��d!", 
    			"Nie znaleziono pliku konfiguracyjnego, nie baw si� w plikach!", 
    			e.getMessage());
    } catch (IOException e) {
    	if(MCNext.debugMode) e.printStackTrace();
    	Logger.popupError("Krytyczny b��d!", 
    			"B��d podczas wczytywania pliku konfiguracyjnego. Powiadom nas o tym!", 
    			e.getMessage());
    } catch (ParseException e) {
    	if(MCNext.debugMode) e.printStackTrace();
    	Logger.popupError("Krytyczny b��d!", 
    			"B��d podczas analizy pliku konfiguracyjnego. Powiadom nas o tym!", 
    			e.getMessage());
    }
  }

  @SuppressWarnings("unchecked")
  public void put(Object key, Object value)
  {
    obj.put(key, value);
  }

  public Object get(Object key) {
    Object ret = null;
    if ((ret = this.obj.get(key)) == null) {
      writeDefaults(key);
      ret = this.obj.get(key);
    }
    return ret;
  }
  
  public Boolean getBoolean(String key) { return (Boolean)get(key); }
  public String getString(String key) { 
	  return (String)get(key); 
  }
}