package tk.piotrek54pl.mcnext;

import javax.swing.JOptionPane;

import com.alee.laf.optionpane.WebOptionPane;
import com.alee.managers.notification.NotificationManager;

public class Logger
{
  public static void info(String m)
  {
    System.out.println("[ Info ] " + m);
  }

  public static void debug(String m) {
    if(MCNext.debugMode) System.out.println("[ Debug ] " + m);
  }

  public static void error(String m) {
    System.out.println("[ FatalError ] " + m);
  }

  public static void warning(String m) {
    System.out.println("[ Warning ] " + m);
  }

  public static void log(String a, String m) {
    System.out.println("[ " + a + " ] " + m);
  }

  public static void notifyInfo(String m) {
    NotificationManager.showNotification(m);
  }

  public static void notifyError(String m) {
    NotificationManager.showNotification(m);
  }

  public static void notifyWarning(String m) {
    NotificationManager.showNotification(m);
  }
  
  public static void poke() {
	  StackTraceElement stack = new Throwable().getStackTrace()[1];
	  debug("POKE! File:"+stack.getFileName()+" Method:"+stack.getMethodName()+" Line:"+stack.getLineNumber());
  }
  
  public static void popupError(String title,String message,String debug) {
	  popupError(title,message,debug, true);
  }
  
  public static void popupError(String title,String message,String debug, boolean closeLauncher) {
	 error(message);
	 String[] options = {"Zamknij"};
	  
      String s ="<html><body width='400'><h1>"+title+"</h1><p>"+message+"<br><br>" + "<p style='color:gray'>"+debug;
      
	  WebOptionPane.showOptionDialog(null, s, title, 0, JOptionPane.ERROR_MESSAGE,null, options, null);
	  if(closeLauncher) MCNext.exit();
  }
}