package tk.piotrek54pl.mcnext;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.text.StrSubstitutor;

import tk.piotrek54pl.mcnext.updater.Library;
import tk.piotrek54pl.mcnext.updater.Version;

  public class StartMinecraft
  {
	  private Version v;


  public void start(final String ver)
  {
	  Thread minecraftStartingThread = new Thread() {
		  public void run() {
			  MCNext.frame.switchToWorking();
			  v = MCNext.md.downloadVersion(ver);
			  MCNext.frame.setMessage("Inicjowanie wersji " + v.getId());
			  
			  unzipNatives();
			  
			  Logger.debug("Uruchamianie minecraft " + v.getId() + " z " + MCNext.prefs.get("memory") + "Mb (" + 
				      MCNext.prefs.get("width") + "x" + MCNext.prefs.get("height") + ") (" + 
				      MCNext.prefs.getString("username") + ")");
			  
			  try {
				  List<String> cmds = new ArrayList<String>();
				  cmds.add(Utils.getJavaPath().getAbsolutePath());
				  cmds.add("-Xmx" + MCNext.prefs.get("memory")+"M");
				  cmds.add("-cp"); cmds.add(constructClasspath());
				  cmds.add("-Djava.library.path="+Utils.getNativesDirectory(v.getId()));
				  cmds.add(v.getMainClass());
				  cmds.addAll(Arrays.asList(parseMinecraftArguments()));
				  cmds.add("-width"); cmds.add(String.valueOf(MCNext.prefs.get("width")));
				  cmds.add("-height"); cmds.add(String.valueOf(MCNext.prefs.get("height")));
				  
				  Iterator i = cmds.iterator();
				  while(i.hasNext()) {
					  System.out.println(i.next());
				  }
				  
				  ProcessBuilder pb = new ProcessBuilder(cmds);
				  pb.directory(Utils.getWorkingDirectory());
				  Process process = pb.start();
				  
				  BufferedReader br = new BufferedReader(new InputStreamReader(process.getErrorStream()));
				  String a;
				  try {
				  	while((a=br.readLine())!=null){
				  	
				  		System.out.println(a);
				  	}
				  } catch (IOException e) {
				  	e.printStackTrace();
				  }

				  
			      MCNext.exit();
			  } catch (IOException e) {
				  if(MCNext.debugMode) e.printStackTrace();
				  Logger.popupError("Krytyczny b��d!", 
			    			"Wyst�pi� nieoczekiwany b��d podczas pr�by uruchomienia minecraft. Powiadom nas o tym!", 
			    			e.getMessage());
			  }
		  }
	  };
	  minecraftStartingThread.start();
  }

  private String constructClasspath()
  {
	  Iterator<Library> i = v.getLibraries().iterator();
	  Logger.debug("Loading "+v.getLibraries().size()+" libraries in classpath.");
	  String cp = "";
	  while (i.hasNext()) {
		  Library lib = (Library)i.next();
		  if ((!lib.containsNatives()) && (lib.getTargetOS().contains(Utils.getOperatingSystem()))) {
			  cp = cp + lib.getFile().getAbsolutePath() + System.getProperty("path.separator");
		  }
	  }
	  cp = cp + Utils.getWorkingDirectory().getAbsolutePath() + File.separator + "versions" + File.separator + this.v.getId() + File.separator + this.v.getId() + ".jar";
	  return cp;
  }

  private String[] parseMinecraftArguments() {
	  Map<String,String> map = new HashMap<String,String>();
	  map.put("auth_username", MCNext.prefs.getString("username"));
	  map.put("auth_player_name", MCNext.prefs.getString("username"));
	  map.put("auth_uuid", "cbaiegac06da4ad7998065fa97e17227");
	  map.put("auth_session", "cbaiegac06da4ad7998065fa97e17227");
	  map.put("auth_access_token", "cbaiegac06da4ad7998065fa97e17227");
	  map.put("version_name", v.getId());
	  map.put("game_directory", Utils.getWorkingDirectory().getAbsolutePath());
	  map.put("game_assets", new File(Utils.getWorkingDirectory(), "assets").getAbsolutePath());
	  map.put("user_properties", "");
	  StrSubstitutor sub = new StrSubstitutor(map);
	  
	  return sub.replace(this.v.getMinecraftArguments()).split(" ");
  }

  private void unzipNatives() {
	  Iterator<Library> i = this.v.getLibraries().iterator();
	  while (i.hasNext()) {
		  Library lib = (Library)i.next();
		  if ((!lib.getTargetOS().contains(Utils.getOperatingSystem())) || (!lib.containsNatives())) continue;
		  try {
			  Utils.unzipFiles(lib.getFile(), Utils.getNativesDirectory(this.v.getId()));
		  } catch (IOException e) {
			  e.printStackTrace();
			  Logger.popupError("Krytyczny b��d!", 
		    			"Nie uda�o si� wypakowa� natives. Powiadom nas o tym!", 
		    			e.getMessage());
		  }
	  }
  }
}