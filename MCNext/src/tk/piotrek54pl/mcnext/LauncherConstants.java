package tk.piotrek54pl.mcnext;

public final class LauncherConstants
{
  public static final String VERSION = "1.0.12";
  public static final String LATEST_STABLE_URL = "http://mc-next.tk/download.php?action=stable-version";
  public static final String LATEST_BETA_URL = "http://mc-next.tk/download.php?action=beta-version";
  public static final String RESOURCES_URL = "http://resources.download.minecraft.net/";
  public static final String DOWNLOAD_URL = "https://s3.amazonaws.com/Minecraft.Download/";

  public static int getNumericVersion()
  {
    return Integer.parseInt(VERSION.replace(".", ""));
  }
}