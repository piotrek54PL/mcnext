package tk.piotrek54pl.mcnext;

import com.alee.laf.WebLookAndFeel;
import com.alee.laf.optionpane.WebOptionPane;

import javax.swing.SwingUtilities;

import tk.piotrek54pl.mcnext.gui.MainFrame;
import tk.piotrek54pl.mcnext.updater.LauncherUpdater;
import tk.piotrek54pl.mcnext.updater.MinecraftDownload;
import tk.piotrek54pl.mcnext.updater.MinecraftResources;

public class MCNext
{
  public static MainFrame frame;
  public static MinecraftResources mr;
  public static MinecraftDownload md;
  public static StartMinecraft sm;
  public static Properties prefs;
  public static LauncherUpdater lu;
  public static boolean offlineMode = true;
  public static boolean debugMode = true;

  public static void main(String[] args)
  {
	  if ((Utils.getOperatingSystem() == OS.MACOS) || (Utils.getOperatingSystem() == OS.OTHER)) {
		  WebOptionPane.showMessageDialog(frame, "Niestety tw�j system operacyjny nie jest wspierany :(", 
				  "Niewspierany system operayjny!", 0);
		  System.exit(0);
	  }
	  
	  offlineMode = !Utils.isInternetConnection();
	  System.out.println("Launcher stworzony przez piotrek54PL :)");
	  System.out.println("Wszelkie prawa zastrze�one.");
	  System.out.println("Wersja: "+LauncherConstants.VERSION);
    
    prefs = new Properties();
    prefs.read();
    mr = new MinecraftResources();
    md = new MinecraftDownload();
    sm = new StartMinecraft();
    lu = new LauncherUpdater();
    
    debugMode = prefs.getBoolean("debugMode");
    
    SwingUtilities.invokeLater(new Runnable()
    {
    	public void run()
    	{
    		WebLookAndFeel.install();
    		MCNext.frame = new MainFrame();
    		MCNext.frame.switchToWorking();
    	}
    });
    try {while (frame==null) Thread.sleep(1); } catch(Exception e) {}
    if (prefs.getBoolean("forceOfflineMode")) offlineMode = true;
    if (offlineMode) MCNext.frame.setTip("Offline mode");
    
    if (prefs.getBoolean("checkResources") && !offlineMode) mr.updateResources();
    md.update();
    frame.setVersions(md.getVersions());
    frame.switchToLogin();
    if (prefs.getBoolean("checkLauncherUpdate") && !offlineMode) lu.updateLauncher();
  }

  public static void exit() {
    prefs.write();
    Logger.info("Launcher zosta� wy��czony.");
    System.exit(0);
  }
}