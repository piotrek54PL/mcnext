package tk.piotrek54pl.mcnext;

public enum OS
{
  WINDOWS("windows"), 
  LINUX("linux"), 
  MACOS("osx"), 
  OTHER("");

  private final String name;

  private OS(String name) { this.name = name; }
  public String getName() { return name; }
}