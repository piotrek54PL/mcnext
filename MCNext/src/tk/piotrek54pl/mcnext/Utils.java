package tk.piotrek54pl.mcnext;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.swing.UIManager;

public class Utils
{
  private static File workingDirectory;
  private static OS operatingSystem;

  public static File getWorkingDirectory()
  {
    if (workingDirectory == null) workingDirectory = generateWorkingDirectory();
    return workingDirectory;
  }

  private static File generateWorkingDirectory() {
    String osName = System.getProperty("os.name").toLowerCase();
    String userHome = System.getProperty("user.home", ".");
    File dir = null;

    if (osName.contains("win")) {
      String applicationData = System.getenv("APPDATA");
      if (applicationData != null)
        dir = new File(applicationData, ".minecraft" + File.separator);
      else
        dir = new File(userHome, ".minecraft" + File.separator);
    } else if ((osName.contains("linux")) || (osName.contains("unix"))) {
      dir = new File(userHome, ".minecraft" + File.separator);
    } else {
      dir = new File(userHome, "minecraft" + File.separator);
    }

    if (!dir.exists()) {
      dir.mkdirs();
    }

    return dir;
  }

  public static File getNativesDirectory(String verId)
  {
    return new File(getWorkingDirectory(), "versions" + File.separator + verId + File.separator + "natives");
  }

  public static OS getOperatingSystem()
  {
    if (operatingSystem == null) operatingSystem = generateOperatingSystem();
    return operatingSystem;
  }

  private static OS generateOperatingSystem() {
    String os = System.getProperty("os.name").toLowerCase();
    if (os.contains("win"))
      return OS.WINDOWS;
    if (os.contains("linux"))
      return OS.LINUX;
    if (os.contains("unix"))
      return OS.LINUX;
    if (os.contains("macos"))
      return OS.MACOS;
    if (os.contains("solaris")) {
      return OS.OTHER;
    }
    return OS.OTHER;
  }

  public static File getJavaPath() {
    String path = System.getProperty("java.home") + File.separator + "bin" + File.separator;

    if (getOperatingSystem() == OS.WINDOWS) {
      return new File(path + "javaw.exe");
    }
    
    return new File(path + "java");
  }

  public static void downloadFile(File target, URL url) throws MalformedURLException, IOException
  {
    URLConnection conn = url.openConnection();
    InputStream is = conn.getInputStream();

    if (!target.getParentFile().exists()) target.getParentFile().mkdirs();
    if (!target.exists()) target.createNewFile();
    if(conn.getContentLength() == target.length()) return;
    FileOutputStream fos = new FileOutputStream(target);

    byte[] buffer = new byte[65536];
    int bytesRead = 0;

    while ((bytesRead = is.read(buffer)) != -1) {
      fos.write(buffer, 0, bytesRead);
    }

    if (fos != null) fos.close();
    if (is != null) is.close(); 
  }

  public static String getURLContent(String url) throws IOException
  {
      return getURLContent(new URL(url));
  }

  public static String getURLContent(URL url) throws IOException
  {
    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    conn.addRequestProperty("User-Agent", "Mozilla/4.76");
    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
    String content = "";
    String inputLine;
    while ((inputLine = br.readLine()) != null)
    {
      content = content + inputLine;
    }
    return content;
  }

  public static void unzipFiles(File sourceFile, File outputDirectory) throws IOException
  {
    if ((!outputDirectory.exists()) && (outputDirectory.isDirectory())) outputDirectory.mkdirs();
    ZipInputStream zis = new ZipInputStream(new FileInputStream(sourceFile));
    ZipEntry entry;
    while ((entry = zis.getNextEntry()) != null)
    {
      File outputFile = new File(outputDirectory, entry.getName());

      if ((!entry.isDirectory()) && (outputFile.getParentFile().getAbsolutePath().equals(outputDirectory.getAbsolutePath()))) {
        if (!outputFile.exists()) {
          outputFile.getParentFile().mkdirs();
          outputFile.createNewFile();
        }

        FileOutputStream fos = new FileOutputStream(outputFile);
        byte[] buffer = new byte[8192];
        int bytesRead = 0;
        while ((bytesRead = zis.read(buffer)) != -1) {
          fos.write(buffer, 0, bytesRead);
        }

        if (fos != null) fos.close();
      }

    }

    zis.closeEntry();
    zis.close();
  }
  
  public static void unzipFile(File zip, String path, File output) {
	  try {
		if (!output.exists()) {
	          output.getParentFile().mkdirs();
	          output.createNewFile();
	        }
		FileOutputStream fos = new FileOutputStream(output);
		FileInputStream fis = new FileInputStream(zip);
		ZipInputStream zis = new ZipInputStream(fis);
		ZipEntry ze = null;
		while ((ze = zis.getNextEntry()) != null) {
			if (ze.getName().equals(path)) {
				byte[] buffer = new byte[8192];
		        int bytesRead = 0;
		        
		        while ((bytesRead = zis.read(buffer)) != -1) {
		            fos.write(buffer, 0, bytesRead);
		        }
		        
		        if (fos != null) fos.close();
		        break;
			}
		}
		
		zis.closeEntry();
	    zis.close();
	    fis.close();
	} catch ( IOException e) {
		if(MCNext.debugMode) e.printStackTrace();
		Logger.popupError("Krytyczny b��d!", 
    			"Wyst�pi� nieoczekiwany b��d podczas rozpakowywania wymaganego pliku. Powiadom nas o tym!", 
    			e.getMessage());
	}
  }

  public static void openURI(String url) throws Exception {
    if (Desktop.isDesktopSupported()) {
      URI uri = new URI(url);
      Desktop.getDesktop().browse(uri);
    } else {
      throw new Exception("Browser launching not supported");
    }
  }

  public static boolean isInternetConnection() {
    try {
      Socket socket = new Socket("74.125.235.70", 80);
      socket.close();
      return true; } catch (Exception e) {}
    return false;
  }
  
  public static void setUIFont (javax.swing.plaf.FontUIResource f){
	    Enumeration<Object> keys = UIManager.getDefaults().keys();
	    while (keys.hasMoreElements()) {
	      Object key = keys.nextElement();
	      Object value = UIManager.get (key);
	      if (value != null && value instanceof javax.swing.plaf.FontUIResource)
	        UIManager.put (key, f);
	      }
  }
  
  public static void copyFolder(File src, File dest)
  {
	  if(src.isDirectory()){
		  String files[] = src.list();
	 
		  for (String file : files) {
			  File srcFile = new File(src, file);
			  File destFile = new File(dest, file);
			  copyFolder(srcFile,destFile);
		  }
	  } else{
		  if(src.length() == dest.length()) return;
		  copyFile(src, dest);
  	  }
  }
  
  public static void copyFile(File src, File dest) {
	  try {
		  if(!dest.exists()){
			  dest.getParentFile().mkdirs();
			  dest.createNewFile();
		  }
		  
		  InputStream in = new FileInputStream(src);
		  OutputStream out = new FileOutputStream(dest); 
	
		  byte[] buffer = new byte[1024];
	
		  int length;
		  while ((length = in.read(buffer)) > 0){
		    	   out.write(buffer, 0, length);
		  }
	
		  in.close();
		  out.close();
	  } catch (IOException e) {
		  if(MCNext.debugMode) e.printStackTrace();
		  Logger.popupError("Krytyczny b��d!", 
	    			"Wyst�pi� nieoczekiwany b��d podczas pr�by skopiowania wymaganego pliku. Powiadom nas o tym!", 
	    			e.getMessage());
	  }
  }
  
  public static int getClosestInt(int of, List<Integer> in) {
	    int min = Integer.MAX_VALUE;
	    int closest = of;

	    for (int v : in) {
	        final int diff = Math.abs(v - of);

	        if (diff < min) {
	            min = diff;
	            closest = v;
	        }
	    }

	    return closest;
	}
}