package tk.piotrek54pl.mcnext.gui;

import com.alee.laf.rootpane.WebFrame;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import tk.piotrek54pl.mcnext.Logger;
import tk.piotrek54pl.mcnext.MCNext;
import tk.piotrek54pl.mcnext.updater.Version;

public class MainFrame extends WebFrame
{
  private static final long serialVersionUID = 1L;
  private LoginPanel loginpanel;
  private WorkingPanel workingpanel;
  private OptionsPanel optionspanel;

  public MainFrame()
  {
    super("MCNext");
    
	try {
		Font f = Font.createFont(Font.TRUETYPE_FONT, getClass().getClassLoader().getResourceAsStream("MyriadProRegular.OTF"));
		f = f.deriveFont(Font.PLAIN,14);
		//Utils.setUIFont(new FontUIResource(f));
	} catch (Exception e) { 
		if(MCNext.debugMode) e.printStackTrace();
		Logger.popupError("Krytyczny b��d!", 
    			"Wyst�pi� nieoczekiwany b��d, nie uda�o si� wczyta� czcionki. Powiadom nas o tym!", 
    			e.getMessage());
	} 
	
    setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    this.addWindowListener(new WindowListener(){
		public void windowActivated(WindowEvent arg0) {}
		public void windowClosed(WindowEvent arg0) {}
		public void windowClosing(WindowEvent arg0) {
			MCNext.exit();
		}
		public void windowDeactivated(WindowEvent arg0) {}
		public void windowDeiconified(WindowEvent arg0) {}
		public void windowIconified(WindowEvent arg0) {}
		public void windowOpened(WindowEvent arg0) {}
    });
    setLayout(null);
    setSize(854, 480);
    setResizable(false);
    setShowMaximizeButton(false);
    setContentPane(new JLabel(new ImageIcon(getClass().getClassLoader().getResource("background.png"))));
    try {
      setIconImage(ImageIO.read(getClass().getClassLoader().getResource("icon.png")));
    } catch (IOException e) {
    	if(MCNext.debugMode) e.printStackTrace();
    	Logger.popupError("Krytyczny b��d!", 
    			"Wyst�pi� nieoczekiwany b��d, nie uda�o si� wczyta� ikony. Powiadom nas o tym!", 
    			e.getMessage());
    }

    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    setLocation(screenSize.width / 2 - getSize().width / 2, screenSize.height / 2 - getSize().height / 2);
    
    add(Box.createHorizontalGlue());
    loginpanel = new LoginPanel();
    add(loginpanel);
    workingpanel = new WorkingPanel();
    add(workingpanel);
    optionspanel = new OptionsPanel();
    add(optionspanel);
    add(Box.createHorizontalGlue());
    
    setVisible(true);
  }

  public void switchToLogin()
  {
    this.loginpanel.setVisible(true);
    if (this.workingpanel.isVisible()) this.workingpanel.setVisible(false);
    if (this.optionspanel.isVisible()) this.optionspanel.setVisible(false); 
  }

  public void switchToWorking()
  {
    this.workingpanel.setVisible(true);
    if (this.loginpanel.isVisible()) this.loginpanel.setVisible(false);
    if (this.optionspanel.isVisible()) this.optionspanel.setVisible(false);
  }

  public void switchToOptions()
  {
    this.optionspanel.setVisible(true);
    if (this.loginpanel.isVisible()) this.loginpanel.setVisible(false);
    if (this.workingpanel.isVisible()) this.workingpanel.setVisible(false); 
  }

  public void setProgress(int percent)
  {
    if (percent >= 100) {
      this.workingpanel.prProgress.setIndeterminate(true);
      this.workingpanel.prProgress.setValue(0);
    }
    else {
      if (!this.workingpanel.prProgress.isVisible()) this.workingpanel.prProgress.setVisible(true);
      this.workingpanel.prProgress.setIndeterminate(false);
      this.workingpanel.prProgress.setValue(percent);
    }
  }

  public void setMessage(String msg) {
    this.workingpanel.lStatus.setText(msg);
  }

  @SuppressWarnings("unchecked")
  public void setVersions(List<Version> versions)
  {
    this.loginpanel.cbVersions.removeAllItems();
    Iterator<Version> i = versions.iterator();
    String selectedVersion = (String)MCNext.prefs.get("selectedVersion");
    
    while (i.hasNext()) {
    	Version v = i.next();
    	if(v.getType().equals("release") && MCNext.prefs.getBoolean("showReleaseVersions") ||
    	   v.getType().equals("snapshot") && MCNext.prefs.getBoolean("showSnapshotVersions") ||
    	   v.getType().equals("old_beta") && MCNext.prefs.getBoolean("showBetaVersions") ||
    	   v.getType().equals("old_alpha") && MCNext.prefs.getBoolean("showAlphaVersions") ||
    	   v.getType().equals("forge") && MCNext.prefs.getBoolean("showForgeVersions")) {
    		
    		String item = v.isDownloaded()+"!"+v.getType()+"!"+v.getId();
        	loginpanel.cbVersions.addItem(item);
        	if ((selectedVersion != null) && (selectedVersion.equalsIgnoreCase(v.getId())))
                this.loginpanel.cbVersions.setSelectedItem(item);
    	}
    }
  }

  public void setTip(String msg) {
    loginpanel.lMessage.setText(msg);
  }
}