package tk.piotrek54pl.mcnext.gui;

import com.alee.laf.button.WebButton;
import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.combobox.WebComboBoxCellRenderer;
import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.text.WebTextField;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;

import tk.piotrek54pl.mcnext.MCNext;

public class LoginPanel extends WebPanel implements ActionListener
{
	private static final long serialVersionUID = 1L;
	public WebTextField tfLogin;
	public WebButton bOptions;
	public WebButton bPlay;
	public WebComboBox cbVersions;
	public WebLabel lMessage;

	@SuppressWarnings("unchecked")
	public LoginPanel()
	{
		setBounds(317, 0, 220, 480);
		setBackground(Color.WHITE);
		setLayout(null);

		JLabel logo = new JLabel(new ImageIcon(getClass().getClassLoader().getResource("mcnext.png")));
		logo.setBounds(0, 0, 220, 80);
		add(logo);
    
		cbVersions = new WebComboBox();
		cbVersions.setBounds(35, 270, 150, 28);
		cbVersions.setRenderer(new WebComboBoxCellRenderer ( cbVersions ){
			private static final long serialVersionUID = 1L;

			@SuppressWarnings("rawtypes")
			@Override
            public Component getListCellRendererComponent ( final JList list, final Object value, final int index, final boolean isSelected, final boolean cellHasFocus )
            {
                final WebLabel renderer = ( WebLabel ) super.getListCellRendererComponent ( list, value, index, isSelected, cellHasFocus );
                
                if(!renderer.getText().equals("")) {
	                String[] parts = renderer.getText().split("!");
	                renderer.setFontStyle(Font.BOLD);
	                
	                if(parts[1].equals("release")) {
	                	renderer.setForeground(Color.decode("#99CC00"));
	                	renderer.setText("Release "+parts[2]);
	                } else if (parts[1].equals("snapshot")) {
	                	renderer.setForeground(Color.decode("#669966"));
	                	renderer.setText("Snapshot "+parts[2]);
	                }  else if(parts[1].equals("old_beta")) {
	                	renderer.setForeground(Color.decode("#7a8972"));
	                	renderer.setText( "Beta "+parts[2].replace("b", ""));
	                } else if(parts[1].equals("old_alpha")) {
	                	renderer.setForeground(Color.decode("#959694"));
	                	renderer.setText( "Alpha "+parts[2].replace("a", ""));
	                }	else if(parts[1].equals("forge")) {
	                	renderer.setForeground(Color.decode("#1e90ff"));
	                	renderer.setText( "Forge "+parts[2].replace("a", ""));
	                } else {
	                	renderer.setText(parts[2]);
	                }
	                
	                if(parts[0].equals("true")) {
	                	renderer.setText("] "+renderer.getText());
	                }
                }
                return renderer;
            }
		});
		add(cbVersions);

		tfLogin = new WebTextField();
		tfLogin.setInputPrompt("Twój nick");
		tfLogin.setText((String)MCNext.prefs.get("username"));
		tfLogin.setBounds(35, 300, 150, 28);
		tfLogin.addFocusListener(new FocusListener(){
			@Override
			public void focusGained(FocusEvent arg0) {}
			@Override
			public void focusLost(FocusEvent arg0) { MCNext.prefs.put("username", tfLogin.getText()); }
		});
		add(tfLogin);

		bOptions = new WebButton("Opcje");
		bOptions.setBounds(35, 330, 60, 28);
		bOptions.addActionListener(this);
		add(bOptions);

    	bPlay = new WebButton("Graj!");
    	bPlay.setBoldFont();
    	bPlay.setBounds(95, 330, 90, 28);
    	bPlay.addActionListener(this);
    	add(bPlay);

    	lMessage = new WebLabel("");
    	lMessage.setHorizontalAlignment(0);
    	lMessage.setBounds(0, 70, 220, 20);
    	lMessage.setForeground(new Color(255, 127, 0));
    	add(lMessage);

    	setVisible(true);
  }

  public void actionPerformed(ActionEvent ae) {
    if (ae.getSource().equals(this.bPlay)) {
      String id = ((String)this.cbVersions.getSelectedItem()).split("!")[2];
      
      if (!this.tfLogin.getText().isEmpty()) {
    	  MCNext.prefs.put("selectedVersion", id);
    	  MCNext.prefs.put("username", tfLogin.getText());
    	  MCNext.sm.start(id);
      }
    }
    else if (ae.getSource().equals(this.bOptions)) {
      MCNext.frame.switchToOptions();
    }
  }
}