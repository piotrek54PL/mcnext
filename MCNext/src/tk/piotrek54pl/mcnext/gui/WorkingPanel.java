package tk.piotrek54pl.mcnext.gui;

import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.progressbar.WebProgressBar;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class WorkingPanel extends WebPanel
{
  private static final long serialVersionUID = 1L;
  public WebLabel lStatus;
  public WebProgressBar prProgress;

  public WorkingPanel()
  {
    setBounds(317, 0, 220, 480);
    setBackground(Color.WHITE);
    setLayout(null);

    JLabel logo = new JLabel(new ImageIcon(getClass().getClassLoader().getResource("mcnext.png")));
    logo.setBounds(0, 0, 220, 80);
    add(logo);

    lStatus = new WebLabel("Uruchamianie...");
    lStatus.setHorizontalAlignment(0);
    lStatus.setBounds(0, 285, 220, 28);
    lStatus.setForeground(new Color(132, 184, 0));
    lStatus.setBoldFont();
    add(lStatus);

    prProgress = new WebProgressBar();
    prProgress.setBounds(0, 310, 220, 17);
    prProgress.setBorder(null);
    prProgress.setRound(0);
    prProgress.setMinimum(0);
    prProgress.setMaximum(100);
    prProgress.setVisible(true);
    prProgress.setIndeterminate(true);
    add(prProgress);

    setVisible(true);
  }
}