package tk.piotrek54pl.mcnext.gui;

import com.alee.laf.button.WebButton;
import com.alee.laf.checkbox.WebCheckBox;
import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.slider.WebSlider;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import tk.piotrek54pl.mcnext.LauncherConstants;
import tk.piotrek54pl.mcnext.MCNext;
import tk.piotrek54pl.mcnext.Utils;

public class OptionsPanel extends WebPanel implements ActionListener
{
	private static final long serialVersionUID = 1L;
	public WebButton bAbort;
	public WebButton bSave;
	public WebLabel lMemory;
	public WebLabel lMemoryInfo;
	public WebSlider slMemory;
	public WebLabel lWidth;
	public WebLabel lWidthInfo;
	public WebSlider slWidth;
	public WebLabel lHeight;
	public WebLabel lHeightInfo;
	public WebSlider slHeight;
	public WebCheckBox chbCheckResources;
	public WebCheckBox chbCheckLauncherUpdate;
	public WebCheckBox chbDemo;
	public WebComboBox cbResolution;
	public WebCheckBox chbForceOffline;
	public WebCheckBox chbShowRelease;
	public WebCheckBox chbShowSnapshot;
	public WebCheckBox chbShowBeta;
	public WebCheckBox chbShowAlpha;
	public WebCheckBox chbShowForge;
	public WebLabel lVersion;

	public OptionsPanel()
	{
		setBounds(40, 0, 766, 480);
		setBackground(Color.WHITE);
		setLayout(null);

		JLabel logo = new JLabel(new ImageIcon(getClass().getClassLoader().getResource("mcnext.png")));
		logo.setBounds(277, 0, 220, 80);
		add(logo);

		bAbort = new WebButton("Anuluj");
		bAbort.setBounds(312, 330, 60, 28);
		bAbort.addActionListener(this);
		add(this.bAbort);

		bSave = new WebButton("Zapisz");
		bSave.setBounds(372, 330, 90, 28);
		bSave.addActionListener(this);
		add(this.bSave);

		lMemory = new WebLabel("Ilo�� pami�ci RAM:");
		lMemory.setBounds(23, 85, 200, 20);
		add(this.lMemory);

		slMemory = new WebSlider(0);
		slMemory.setBounds(20, 95, 300, 40);
		slMemory.setMinimum(128);
		com.sun.management.OperatingSystemMXBean os = (com.sun.management.OperatingSystemMXBean)
			     java.lang.management.ManagementFactory.getOperatingSystemMXBean();
		long maximum = os.getFreePhysicalMemorySize() /1024 /1024;
		if (maximum > 4096L) maximum = 4096L;
		slMemory.setMaximum((int)maximum);
		slMemory.setMinorTickSpacing(64);
		slMemory.setMajorTickSpacing(512);
		slMemory.setPaintTicks(true);
		slMemory.setSnapToTicks(true);
		slMemory.setValue(((Number)MCNext.prefs.get("memory")).intValue());
		slMemory.addChangeListener(new ChangeListener() {
			 public void stateChanged(ChangeEvent ev) {
				 OptionsPanel.this.lMemoryInfo.setText(OptionsPanel.this.slMemory.getValue() + " MB");
			 }
		 });
		 add(slMemory);

		 lMemoryInfo = new WebLabel(this.slMemory.getValue() + " MB", 4);
		 lMemoryInfo.setBounds(117, 85, 200, 20);
		 add(this.lMemoryInfo);
		
		 
		 lWidth = new WebLabel("Szeroko�� okna:");
		 lWidth.setBounds(23, 140, 200, 20);
		 add(lWidth);
		 
		 Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		 slWidth = new WebSlider(0);
		 slWidth.setBounds(20,148, 230, 40);
		 slWidth.setMinimum(128);
		 slWidth.setMaximum((int)screenSize.width);
		 slWidth.setMinorTickSpacing(2);
		 slWidth.setMajorTickSpacing(64);
		 slWidth.setSnapToTicks(true);
		 slWidth.setValue(((Number)MCNext.prefs.get("width")).intValue());
		 slWidth.addChangeListener(new ChangeListener() {
			 public void stateChanged(ChangeEvent ev) {
				 OptionsPanel.this.lWidthInfo.setText(OptionsPanel.this.slWidth.getValue() + "px");
			 }
		 });
		 add(slWidth);

		 lWidthInfo = new WebLabel(slWidth.getValue() + "px", WebLabel.RIGHT);
		 lWidthInfo.setBounds(66, 140, 180, 20);
		 add(lWidthInfo);
		 
		 
		 lHeight = new WebLabel("Wysoko�� okna:");
		 lHeight.setBounds(23, 195, 200, 20);
		 add(lHeight);
		 
		 slHeight = new WebSlider(0);
		 slHeight.setBounds(20,203, 230, 40);
		 slHeight.setMinimum(128);
		 slHeight.setMaximum((int)screenSize.height);
		 slHeight.setMinorTickSpacing(2);
		 slHeight.setMajorTickSpacing(64);
		 slHeight.setSnapToTicks(true);
		 slHeight.setValue(((Number)MCNext.prefs.get("height")).intValue());
		 slHeight.addChangeListener(new ChangeListener() {
			 public void stateChanged(ChangeEvent ev) {
				 OptionsPanel.this.lHeightInfo.setText(OptionsPanel.this.slHeight.getValue() + "px");
			 }
		 });
		 add(slHeight);

		 lHeightInfo = new WebLabel(slHeight.getValue() + "px", WebLabel.RIGHT);
		 lHeightInfo.setBounds(66, 195, 180, 20);
		 add(lHeightInfo);
		 
		 String[] items = { "360p", "480p", "720p", "1080p"};
		 cbResolution = new WebComboBox(items);
		 cbResolution.setBounds(255,180,65,25);
		 List<Integer> ints = new ArrayList<Integer>();
		 ints.add(360); ints.add(480);  ints.add(720); ints.add(1080);
		 cbResolution.setSelectedItem(Utils.getClosestInt(slHeight.getValue(),ints)+"p");
		 cbResolution.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				int height = Integer.parseInt(((String)cbResolution.getSelectedItem()).replace("p", ""));
				slHeight.setValue(height);
				slWidth.setValue((int) Math.round(1.77777777778 * height));
			}
		 });
		 add(cbResolution);
		 
		
		 chbCheckResources = new WebCheckBox("Sprawdzaj resoruces przy starcie");
		 chbCheckResources.setBounds(450, 80, 300, 20);
		 chbCheckResources.setSelected(((Boolean)MCNext.prefs.get("checkResources")).booleanValue());
		 add(chbCheckResources);

		 chbCheckLauncherUpdate = new WebCheckBox("Sprawdzaj aktualizacje launchera przy starcie");
		 chbCheckLauncherUpdate.setBounds(450, 105, 300, 20);
		 chbCheckLauncherUpdate.setSelected(((Boolean)MCNext.prefs.get("checkLauncherUpdate")).booleanValue());
		 add(chbCheckLauncherUpdate);
		 
		 chbDemo = new WebCheckBox("Uruchom minecraft w trybie demo");
		 chbDemo.setBounds(450, 155, 300, 20);
		 chbDemo.setSelected(MCNext.prefs.getBoolean("demo"));
		 add(chbDemo);
		 
		 chbForceOffline = new WebCheckBox("Wymu� tryb offline launchera");
		 chbForceOffline.setBounds(450, 180, 300, 20);
		 chbForceOffline.setSelected(MCNext.prefs.getBoolean("forceOfflineMode"));
		 add(chbForceOffline);
		 
		 
		 chbShowRelease = new WebCheckBox("Wy�wietlaj wersje Release");
		 chbShowRelease.setBounds(450, 205, 300, 20);
		 chbShowRelease.setSelected(MCNext.prefs.getBoolean("showReleaseVersions"));
		 add(chbShowRelease);
		 
		 chbShowSnapshot = new WebCheckBox("Wy�wietlaj wersje Snapshot");
		 chbShowSnapshot.setBounds(450, 230, 300, 20);
		 chbShowSnapshot.setSelected(MCNext.prefs.getBoolean("showSnapshotVersions"));
		 add(chbShowSnapshot);
		 
		 chbShowBeta = new WebCheckBox("Wy�wietlaj wersje Beta");
		 chbShowBeta.setBounds(450, 255, 295, 20);
		 chbShowBeta.setSelected(MCNext.prefs.getBoolean("showBetaVersions"));
		 add(chbShowBeta);
		 
		 chbShowAlpha = new WebCheckBox("Wy�wietlaj wersje Alpha");
		 chbShowAlpha.setBounds(450, 280, 300, 20);
		 chbShowAlpha.setSelected(MCNext.prefs.getBoolean("showAlphaVersions"));
		 add(chbShowAlpha);
		 
		 chbShowForge = new WebCheckBox("Wy�wietlaj wersje Forge");
		 chbShowForge.setBounds(450, 305, 300, 20);
		 chbShowForge.setSelected(MCNext.prefs.getBoolean("showForgeVersions"));
		 add(chbShowForge);
		 
		 lVersion = new WebLabel("v"+LauncherConstants.VERSION);
		 lVersion.setBounds(20, 420, 180, 20);
		 lVersion.setForeground(new Color(190,190,190));
		 add(lVersion);
		 
		setVisible(true);
	}

	public void actionPerformed(ActionEvent ae) {
		if (ae.getSource().equals(this.bAbort)) {
			MCNext.frame.switchToLogin();
		}
		else if (ae.getSource().equals(this.bSave)) {
			MCNext.prefs.put("memory", Integer.valueOf(this.slMemory.getValue()));
			MCNext.prefs.put("checkResources", Boolean.valueOf(this.chbCheckResources.isSelected()));
			MCNext.prefs.put("checkLauncherUpdate", Boolean.valueOf(this.chbCheckLauncherUpdate.isSelected()));
			MCNext.prefs.put("width",Integer.valueOf(this.slWidth.getValue()));
			MCNext.prefs.put("height",Integer.valueOf(this.slHeight.getValue()));
			MCNext.prefs.put("demo", Boolean.valueOf(this.chbDemo.isSelected()));
			MCNext.prefs.put("forceOfflineMode", Boolean.valueOf(this.chbForceOffline.isSelected()));
			MCNext.prefs.put("showReleaseVersions", Boolean.valueOf(this.chbShowRelease.isSelected()));
			MCNext.prefs.put("showSnapshotVersions", Boolean.valueOf(this.chbShowSnapshot.isSelected()));
			MCNext.prefs.put("showBetaVersions", Boolean.valueOf(this.chbShowBeta.isSelected()));
			MCNext.prefs.put("showAlphaVersions", Boolean.valueOf(this.chbShowAlpha.isSelected()));
			MCNext.prefs.put("showForgeVersions", Boolean.valueOf(this.chbShowForge.isSelected()));
			
			MCNext.frame.setVersions(MCNext.md.getVersions());
			
			MCNext.prefs.write();
			MCNext.frame.switchToLogin();
		}
	}
}